# mysql login

_Emrullah İLÇİN_

## Login without password; 

Create '~/.my.cnf' file;  ex: password '12345678'

`echo -e "[client]\npassword='12345678'" > ~/.my.cnf`

Grant owner access

`chmod 600 ~/.my.cnf`

## Login; 

ex: account name 'emr';

`mysql -uemr`

## Dump DB; 

ex: DB name 'dev'; ex: account name 'emr'

`mysqldump  --skip-extended-insert -uemr --databases dev > dev.sql`

## Restrore DB; 

ex: DB name 'dev'; ex: account name 'emr'

`mysql -uemr dev < dev.sql`

## Full dump with storage procedure 

```bash
#!/bin/bash
## -----------------------------
## ATTENTION; run code with bash 
## ----------------------------

MyUSER="emr"       # Username; ex emr
MyGROUP="emr"			# Groups; ex emr 
#MyPASS="XXX" # entered  ~/.my.cnf file
MyHOST="localhost" # Hostname
HOST="$(hostname)" # Get hostname
 
# Linux bin paths
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
CHOWN="$(which chown)"
CHMOD="$(which chmod)"
GZIP="$(which gzip)"
 
DEST="/home/emr/Backup" # Backup Dest directory; ex: path
 
MBD="$DEST/mysql" # Main directory where backup will be stored
 
NOW="$(date +'%Y-%m-%d')"  # Get data in yyyy-mm-dd format

# DO NOT BACKUP these databases
IGGY="mysql information_schema performance_schema"

if [ ! -d $DEST ]; then
  mkdir -p $DEST
fi

if [ ! -d $MBD ] ; then
  mkdir -p $MBD 
fi

$CHOWN $MyUSER.$MyGROUP -R $DEST   # Only $MyUSER.$MyGROUP can access it
$CHMOD 0700 $DEST  	# change mod

# Get all database list first
DBS="$($MYSQL -u $MyUSER -h $MyHOST -Bse 'show databases')"
 
# Main loop begin 
for db in $DBS
do
	# ---Skip db begin --- 
	skipdb=-1
	if [ "$IGGY" != "" ]; then
		for i in $IGGY
		do
			[ "$db" == "$i" ] && skipdb=1 || :
		# echo " DB $i"
		done
	fi
	# ---Skip db end ---

	# --- Dump begin ---
	if [ "$skipdb" == "-1" ] ; then
		FILE="$MBD/$db.$NOW.gz"
		FILE_SP="$MBD/$db.$NOW.sp"
		echo "Dump DB $FILE"
		# $MYSQLDUMP -u $MyUSER -h $MyHOST -p$MyPASS $db | $GZIP -9 > $FILE
		$MYSQLDUMP -u $MyUSER -h $MyHOST --skip-extended-insert $db | $GZIP -9 > $FILE
						
		$MYSQLDUMP -u $MyUSER -h $MyHOST --routines --no-create-info --no-data --no-create-db --skip-opt $db > $FILE_SP
	fi
	# --- Dump end ---		
done
```

