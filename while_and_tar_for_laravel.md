# bash ex

## while and tar; for laravel archive

```
#!/bin/bash

# use; "bash yedek.sh"
# Emrullah İLÇİN

list=$(ls -d */)

while read -r i; do
   echo ${i%%/};  # default ${i} ;  %% : deleted slash
   tar -cvf yedek.${i%%/}.$(date +"%Y-%m-%d").tar --exclude="node_modules"  --exclude="vendor" --exclude="storage"  ./${i%%/}
done <<< "$list"
```